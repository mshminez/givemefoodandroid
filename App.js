import React, { Component } from 'react';
import { createStackNavigator, createAppContainer } from 'react-navigation';
import Login from './src/screens/Login';
import Userhome from './src/screens/userscreen/Usertab';
import UserResDetail from './src/screens/userscreen/UserResDetail';
import UserResFoodDetail from './src/screens/userscreen/UserResFoodDetail';
import Reshome from './src/screens/resscreen/Reshome';


const AppNavigator = createStackNavigator(
  {
    Login: {
      screen: Login,
      navigationOptions: {
        header: null
      }
    },
    Userhome: {
      screen: Userhome,
      navigationOptions: {
        header: null
      }
    },
    Reshome: {
      screen: Reshome,
      navigationOptions: {
        header: null
      }
    },
    UserResDetail: {
      screen: UserResDetail,
      
      navigationOptions: {
        title: 'Detail',
        
      }
    },
    UserResFoodDetail: {
      screen: UserResFoodDetail,
      }
  },

  {
    initialRouteName: 'Login',

  }
);

const AppContainer = createAppContainer(AppNavigator);

export default class App extends Component {
  render() {
    return <AppContainer />;
  }
}