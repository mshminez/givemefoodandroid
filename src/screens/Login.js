import React, { Component } from 'react';  
import {StyleSheet,
  Text,
  View,
  TextInput,
  Button,
  TouchableHighlight,
  Image,
  ImageBackground,
  Alert } from 'react-native';
import Reshome from './resscreen/Reshome';

export default class Login extends Component {  
  constructor(props) {
    super(props);
    state = {
      fullName: '',
      email   : '',
      password: '',
    }
  }
  onClickListener = (viewId) => {
    Alert.alert("Alert", "Coming soon");
  }

  render() {
    return (
      
      <View style={styles.container}>
      <Image source={require('../images/Logo.png')} style={{width: 250, height: 250}} />
      <View style={styles.inputContainer}>
        <Image style={styles.inputIcon} source={{uri: 'https://img.icons8.com/office/16/000000/gender-neutral-user.png'}}/>
        <TextInput style={styles.inputs}
            placeholder="Username"
            keyboardType="email-address"
            underlineColorAndroid='transparent'
            onChangeText={(fullName) => this.setState({fullName})}/>
      </View>

      <View style={styles.inputContainer}>
        <Image style={styles.inputIcon} source={{uri: 'https://img.icons8.com/office/16/000000/key-security.png'}}/>
        <TextInput style={styles.inputs}
             placeholder="Password"
             secureTextEntry={true}
             underlineColorAndroid='transparent'
            onChangeText={(email) => this.setState({email})}/>

      </View>
        <TouchableHighlight style={[styles.buttonContainer, styles.signupButton]} onPress={() => this.onClickListener('sign_up')}>
          <Text style={styles.signUpText}>Login</Text>
        </TouchableHighlight>


        <TouchableHighlight style={[styles.buttonContainer, styles.UserhomeButton]} onPress={() => this.props.navigation.navigate('Userhome')}>
          <Text style={styles.signUpText}>UserHome</Text>
        </TouchableHighlight>

        
        <TouchableHighlight style={[styles.buttonContainer, styles.ReshomeButton]} onPress={() => this.props.navigation.navigate('Reshome')}>
          <Text style={styles.signUpText}>RESHome</Text>
        </TouchableHighlight>
  

      </View>
    );
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#ffdeaa',
  },
  inputContainer: {
      borderBottomColor: '#F5FCFF',
      backgroundColor: '#FFFFFF',
      borderRadius:30,
      borderBottomWidth: 1,
      width:250,
      height:45,
      marginBottom:20,
      flexDirection: 'row',
      alignItems:'center'
  },
  inputs:{
      height:45,
      marginLeft:16,
      borderBottomColor: '#FFFFFF',
      flex:1,
  },
  inputIcon:{
    width:30,
    height:30,
    marginLeft:15,
    justifyContent: 'center'
  },
  buttonContainer: {
    height:45,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom:20,
    width:250,
    borderRadius:30,
  },
  signupButton: {
    backgroundColor: "#ffa211",
  },
  UserhomeButton: {
    backgroundColor: "#69D81A",
  },
  ReshomeButton:{
    backgroundColor:"#1AA9D8",
  },
  signUpText: {
    color: 'black',
  },
  
});
 