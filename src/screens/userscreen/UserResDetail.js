import React, { Component } from "react";
import { Text, View, StyleSheet, Image, TouchableOpacity, TouchableHighlight } from "react-native";
import { Icon, Header, Left, Right, Title, Body, Footer } from "native-base";
import FoodCard from '../FoodCard'
import { ScrollView } from "react-native-gesture-handler";
import ItemComponent from './ItemComponent';
import { db } from './config';


export default class UserResDetail extends Component {

    render(props) {
        return (
            <View style={styles.container}>

                <Image source={{ uri: 'http://halalinthailand.com/wp-content/uploads/2018/02/IMG_0216-1024x682.jpg' }} style={styles.imagestyle} />



                <View style={styles.detailstyle}>


                    <Text>Detail :</Text>

                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <Icon type="Entypo" name="location-pin" style={styles.textDetail} />
                        <Text style={styles.textDetail}>my home</Text>
                    </View>

                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <Icon type="Entypo" name="location-pin" style={styles.textDetail} />
                        <Text style={styles.textDetail}>my mom</Text>
                    </View>

                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <Icon type="Entypo" name="location-pin" style={styles.textDetail} />
                        <Text style={styles.textDetail}>my dick</Text>
                    </View>


                </View>

                <Text style={styles.textDetail} >Food List</Text>

                <View style={styles.foodstyle}>

                    <ScrollView horizontal={true}>
                        <FoodCard navigation={this.props.navigation} />
                        <FoodCard navigation={this.props.navigation} />
                        <FoodCard navigation={this.props.navigation} />
                        <FoodCard navigation={this.props.navigation} />
                        <FoodCard navigation={this.props.navigation} />
                    </ScrollView>
                </View>


            </View>
        );
    }
}

const styles = StyleSheet.create({

    container: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: '#ffdeaa'

    },
    
    detailstyle: {
        width: '95%',
        height: '20%',
        margin: 10,
        backgroundColor: 'orange',
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 5 },
        shadowOpacity: 0.3,
        shadowRadius: 3.5,
        elevation: 5,
        justifyContent: 'space-around',
        padding: 10,
        paddingLeft: 20
    },
    imagestyle: {
        width: '100%',
        height: '33%',
        backgroundColor: '#ffdeEE'

    },

    foodstyle: {
        width: '95%',
        height: '30%',
        margin: 5,
        backgroundColor: 'orange',
        paddingLeft: 10,
        paddingRight: 10,
        justifyContent: 'space-between',
        flexDirection: 'row',
    },

    foodcardstyle: {
        flexDirection: 'row',
    },

    textDetail: {
        fontSize: 15,
        color: '#2b2b2b'
    },

})