import React, { Component } from 'react';
import { AppRegistry, FlatList, StyleSheet, Text, View, ScrollView, Image, SectionList } from 'react-native';
import Card from '../Card'

export default class UserStorelist extends Component {

  render() {
    return (
      <View style={styles.container} >
        <ScrollView>
          <Card navigation= {this.props.navigation} />
        </ScrollView>

      </View>
    );
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 20,

  },
  item: {
    padding: 10,
    fontSize: 18,
    height: 44,
  },
})