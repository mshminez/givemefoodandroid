import React, { Component } from "react";
import { Modal, Text, View, StyleSheet, Image, TouchableOpacity, TouchableHighlight, Alert } from "react-native";
import { Icon, Header, Left, Right, Title, Body, Footer } from "native-base";
export default class UserResFoodDetail extends Component {
    constructor(props) {
        super(props);
    }

    state = {
        modalVisible: false,
    };

    setModalVisible(visible) {
        this.setState({ modalVisible: visible });
    }

    render(props) {
        return (
            <View style={styles.container}>

                <Image source={{ uri: 'https://loremflickr.com/320/240' }} style={styles.imagestyle} />



                <View style={styles.detailstyle}>


                    <Text style={{ fontSize: 25 }}>Detail :</Text>

                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>

                        <Text style={styles.textDetail}>ทำอะไรก็ได้ก็ไม่ได้ขอตังใคร </Text>
                    </View>

                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>

                        <Text style={styles.textDetail}>ได้แต่มอง มองอะไร</Text>
                    </View>

                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>

                        <Text style={styles.textDetail}>my dick</Text>
                    </View>


                </View>

                <Footer style={styles.buttonStyle}>

                    <Modal animationType="slide" transparent={false} visible={this.state.modalVisible} >
                        <View style={styles.modalContainer}>
                            <View style={styles.innerContainer}>
                                <Text style={{ fontSize: 25 }}>Food Ingredients</Text>

                                <View style={{ flexDirection: 'row', alignItems: 'center' }}>

                                    <Text style={styles.textDetail}>ทำอะไรก็ได้ก็ไม่ได้ขอตังใคร </Text>
                                </View>


                                <TouchableHighlight style={[styles.buttonContainer, styles.signupButton]} onPress={() => { this.setModalVisible(!this.state.modalVisible); }}>
                                    <Text style={styles.signUpText}>Back to Food Detail</Text>
                                </TouchableHighlight>

                            </View>
                        </View>

                    </Modal>

                    <TouchableHighlight style={[styles.buttonContainer, styles.signupButton]} onPress={() => { this.setModalVisible(true); }}>
                        <Text>Show Ingredient</Text>
                    </TouchableHighlight>

                    <TouchableHighlight style={[styles.buttonContainer, styles.UserhomeButton]} onPress={() => this.props.navigation.navigate('Userhome')}>
                        <Text style={styles.signUpText}>Add to card</Text>
                    </TouchableHighlight>

                </Footer>


            </View>
        );
    }
}

const styles = StyleSheet.create({

    buttonStyle: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: '#ffdeaa',
        flexDirection: 'row',
        padding: 10,
        paddingLeft: 20,
        width: '100%',
        height: '15%',
        justifyContent: 'space-around',

    },

    buttonContainer: {
        height: 40,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        width: 150,
        borderRadius: 5,
    },

    signupButton: {
        backgroundColor: "#ffa211",
    },
    UserhomeButton: {
        backgroundColor: "#69D81A",
    },

    container: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: '#ffdeaa'
    },

    modalContainer: {
        flex: 1,
        justifyContent: 'center',
        backgroundColor: '#ffdeaa',
    },
    innerContainer: {
        justifyContent: 'center',
        alignItems: 'center',

    },

    detailstyle: {
        width: '98%',
        height: '45%',
        margin: 10,
        backgroundColor: 'orange',
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 5 },
        shadowOpacity: 0.3,
        shadowRadius: 3.5,
        elevation: 5,
        justifyContent: 'flex-start',
        padding: 10,
        paddingLeft: 15
    },
    imagestyle: {
        width: '100%',
        height: '40%',
        backgroundColor: '#ffdeEE'

    },

    foodstyle: {
        width: '95%',
        height: '35%',
        margin: 10,
        backgroundColor: 'orange',
        padding: 10,
        paddingLeft: 20,
        padding: 10,
        paddingLeft: 20,
        justifyContent: 'space-between',
        flexDirection: 'row',
    },

    foodcardstyle: {
        flexDirection: 'row',
    },

    textDetail: {
        fontSize: 15,
        color: '#2b2b2b'
    },

})