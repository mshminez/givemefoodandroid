import React, { Component } from 'react';  
import {  
  View,
  Text,
  TouchableHighlight,
  StyleSheet,
  TextInput,
  AlertIOS
} from 'react-native';
import {db} from '../userscreen/config'
import ItemComponent from './ItemComponent';

let itemsRef = db.ref('/promotion')
let addItem = item => {  
  db.ref('/promotion').push({
    name: item
  });
};


export default class Screen3 extends Component {
  state = {
    name: ''
  };
  state = {
    items: []
  };

  componentDidMount() {
    itemsRef.on('value', snapshot => {
      let data = snapshot.val();
      let items = Object.values(data);
      this.setState({ items });
    });
  }


  handleChange = e => {
    this.setState({
      name: e.nativeEvent.text
    });
  };
  handleSubmit = () => {
    addItem(this.state.name);
    AlertIOS.alert('Item saved successfully');
  };
  



  render() {
    return (
      <View style={{ flex: 1}}>
        <Text style={styles.title}>Add Promotion</Text>
        <TextInput style={styles.itemInput} onChange={this.handleChange} />
        <TouchableHighlight
          style={styles.button}
          underlayColor="white"
          onPress={this.handleSubmit}

        >
          <Text style={styles.buttonText}>ADD</Text>
        </TouchableHighlight>
        <View style = {{ flex: 2}}>
        <Text style={styles.title}>Current Promotion</Text>
        {this.state.items.length > 0 ? (
          <ItemComponent items={this.state.items} />
        ) : (
          <Text>No items</Text>
        )}
        </View>
      </View>
      
    );
  }
}

const styles = StyleSheet.create({  
  main: {
    flex: 1,
    padding: 30,
    flexDirection: 'column',
    justifyContent: 'center',
    backgroundColor: '#F98742'
  },
  title: {
    marginBottom: 20,
    fontSize: 25,
    textAlign: 'center'
  },
  itemInput: {
    height: 50,
    padding: 4,
    marginRight: 5,
    fontSize: 23,
    borderWidth: 1,
    borderColor: '#F98742',
    borderRadius: 8,
    color: '#8B8B8B'
  },
  buttonText: {
    fontSize: 18,
    color: '#111',
    alignSelf: 'center'
  },
  button: {
    height: 45,
    flexDirection: 'row',
    backgroundColor: '#F98742',
    borderColor: '#F98742',
    borderWidth: 1,
    borderRadius: 8,
    marginBottom: 10,
    marginTop: 10,
    alignSelf: 'stretch',
    justifyContent: 'center'
  }
});