import React, { Component } from "react";
import { Text, View, StyleSheet, Image, TouchableOpacity, Button,TouchableHighlight } from "react-native";

class FoodCard extends Component {
  constructor(props) {
    super(props);
  }
  static navigationOptions = {
    header: null
  }

  render(props) {
    return (

      <View>
        <TouchableOpacity style={{ flex: 1 }} onPress={() => this.props.navigation.navigate('UserResFoodDetail')}>
          <View style={{ flex: 1, marginHorizontal: 2, marginTop: 2 }}>
            <View style={{ flex: 1, flexDirection: "row" }}>
              <View style={{ flex: 1, margin: 2 }}>
                <Text style={{ fontSize: 18, fontWeight: "500" }}>
                  กระเพรา
              </Text>
              </View>
            </View>
            <View>
              <Image source={{ uri: 'https://loremflickr.com/320/240' }} style={styles.imgStyle} />
            </View>
            <Text
              numberOfLines={2}
              style={{
                flex: 1,
                marginVertical: 2,
                fontSize: 17,
                fontWeight: "500"
              }}
            >
              Name
          </Text>
            <View style={{ marginTop: 2 }}>
              <Text style={{ color: "#2b2b2b", lineHeight: 21 }}>
                Description
            </Text>
            </View>
            <View style={{ marginBottom: 2 }} />
          </View>
        </TouchableOpacity>

        
      </View>
    );
  }
}

const styles = StyleSheet.create({
  
  imgStyle: {
    height: 100,
    width: 100,
    marginVertical: 2,
    marginRight : 10,
    marginTop : 5
  },
  head: {
    backgroundColor: "white",
    marginHorizontal: 5,
    marginTop: 5
  },
  seperateLine: {
    borderBottomColor: "#d3d3d3",
    borderBottomWidth: 0.5,
    marginTop: 5
  },
  
  placeTitle: {
    marginTop: 5,
    color: "red",
    fontSize: 18
  },
  placeDescription: {
    marginLeft: 5,
    marginRight: 5,
    marginBottom: 5,
    color: "grey",
    fontSize: 12
  },
  textHeader: {
    color: "grey",
    fontSize: 14
  },
  profile: {
    height: '10%',
    width: '15%',

  },
  nameText: {
    fontSize: 14,
    color: "red",
    fontWeight: "bold"
  }
});

export default FoodCard;